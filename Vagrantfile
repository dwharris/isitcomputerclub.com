# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|
  config.vm.box = "chef/centos-7.0"

  config.vm.network "forwarded_port", guest: 80, host: 8080
  config.vm.network "forwarded_port", guest: 443, host: 8443

  config.vm.synced_folder "app/", "/srv/app"

  # Configure settings for virtualbox vm
  config.vm.provider "virtualbox" do |v|
    v.memory = 512
    v.cpus = 2
  end

  # Configure settings for VMware
  config.vm.provider "vmware_fusion" do |vw|
    vw.vmx["memsize"] = "4096"
    vw.vmx["numvcpus"] = "2"
  end

  config.vm.provision "shell", inline: <<-SHELL
    rpm --import http://mirror.centos.org/centos/RPM-GPG-KEY-CentOS-7
    yum install deltarpm -y
    yum upgrade -y
    yum install epel-release -y
    yum install npm nodejs nginx -y
    npm install -g express pm2
    systemctl enable nginx.service
    systemctl start nginx.service
    sudo env PATH=$PATH:/usr/bin pm2 startup centos -u vagrant
    su -c "chmod +x /etc/init.d/pm2-init.sh; chkconfig --add pm2-init.sh"
    cd /srv/app; pm2 start bin/www; cd -
    pm2 save
  SHELL

  config.vm.provision "shell", path: "provision/copy_config.sh"
end
