#!/bin/bash

cp /vagrant/provision/nginx.conf /etc/nginx/nginx.conf
cp /vagrant/provision/isitcomputerclub.com /etc/nginx/conf.d/isitcomputerclub.com

systemctl restart nginx.service