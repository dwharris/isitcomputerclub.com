var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    var date = new Date();
    var computer_club = ""
    if (date.getDay() == 3)
        computer_club = "yes";
    else
        computer_club = "no";
  res.render('index', { title: computer_club, date: date });
});

module.exports = outer;
